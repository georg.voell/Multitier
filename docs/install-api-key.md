## Manually attach API key to User

The public key in PEM format of the login user is displayed. Before you confirm with "yes" again, copy this as an API key to the user.

![setup-tools-keys](pictures/setup-tools-keys.png)
This manual step cannot be avoided if you don't have the rights for user administration: Select your user in the web GUI, select the API key and copy the key into the corresponding field using cut-copy-paste. Save the key and enter "yes" again on the Cloud Shell. A check is made to ensure that the correct user has been selected and that the API key has been installed correctly.

![setup-tools-pem](pictures/setup-tools-pem.png)

[<<](setup.md)
