## Automatic setup of your project environment via CloudShell and Admin-Scripts Tooling Framework

You can easily set up a complete development / project environment in CloudShell. Here you can see the individual steps to make it work.

Log into your OCI environment and click on the CloudShell symbol in the menu bar: <img src="pictures/CloudShell-Icon.png" width="300"><br>
In the CloudShell enter the command "curl -skL https://standby.cloud/download/latest/install-scripts | bash". For more information about the Tooling Framework check out: https://standby.cloud/download/

![Install-Tooling](pictures/Install-Tooling.png)
The tooling framework consists of useful BASH scripts that support you when working with OCI. After the installation, please run the command "source .bashrc" so that the scripts are known in the PATH and the man pages can also be found. With the newly installed "setup-tools" script, tools that have not yet been installed are reloaded or updated and initialized.
```
curl -skL https://standby.cloud/download/latest/install-scripts | bash
source .bashrc
setup-tools
```
![setup-tools-update](pictures/setup-tools-update.png)
After calling "setup-tools", it checks the installed tools and their version. If a tool is not installed or a newer version is available, it will be installed later if this is confirmed with "yes".

![setup-tools-installed](pictures/setup-tools-installed.png)
The next step is to configure various tools. Confirm this also by entering "yes". First, "setup-tools" tries to find out the name of the CloudShell user (your login name). If this is successful, enter "yes" again - otherwise enter "no" and select the correct login user. A private / public key pair is created for the login user and then also for the tenant name (can be used for ssh). 

![setup-tools-keys](pictures/setup-tools-auto-configured.png)
If you don't have the privileges to manage users, it is not possible to attach the API key automatically to the User. In this case please follow [this document](install-api-key.md).

The next step is to select a compartment that Terraform will use to set up the resources. Alternatively, if one does not exist, you can create a new compartment (below direct under the root compartment). Enter "No" to create a new compartment - otherwise "Yes" again. Only one compartment can be selected (under the root compartment any sub-compartment).

![setup-tools-compartment](pictures/setup-tools-compartment.png)
Select the compartment by it's number and confirm with "yes". The selected compartment is displayed and a bucket with the compartment name is created to store e.g. the terraform state file.

![setup-tools-configured](pictures/setup-tools-configured.png)
The tools are now all configured. All config files are stored in the "~/.oci" folder. Execute the command "source .admintools" (this will source the profile script) so that the environment variables are set. If you want, you could add this to your .bash.rc file to automatically execute "source .admintools" when you start the Cloud Shell. Next we could automatically download all the terraform scripts from here.

![setup-tools-setup](pictures/setup-tools-setup.png)
To apply the terraform defined resources, cd to the download folder, optionally edit the parameter file "terraform.tfvars", init terraform and apply the plan.
```
source .admintools
cd multitier
terraform init
terraform apply
```

Instead of working with terraform commands directly, one could use for example "setup-tools terraform apply" to create the resources in one single call. All steps from "setup-tools" can be repeated as often as you like. If you pass a parameter with "setup-tools", you can carry out a single step, e.g .:

setup-tools update<br>
setup-tools config<br>
setup-tools setup<br>

There is a "man-page" and help for all scripts if you pass the parameter "--help". To remove all the scripts call "setup-tools remove". You have to delete all applied resources first with "setup-tools terraform destroy".<br>

[<<](../README.md) | [>>](config.md)
