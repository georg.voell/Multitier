# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

resource "oci_identity_dynamic_group" "monitored_instances" {
	compartment_id = var.tenancy_ocid
	description    = "Dynamic group that includes all monitored instances in tenancy"
	name           = "Monitored-Instances"
	matching_rule  = "any {instance.compartment.id = '${var.compartment_ocid}'}"
}

resource "oci_identity_dynamic_group" "monitored_resources" {
	compartment_id = var.tenancy_ocid
	description    = "Dynamic group that includes all MGMT-Agents in tenancy"
	name           = "Monitored-Resources"
	# matching_rule  = "all {resource.type='managementagent', resource.compartment.id = '${var.compartment_ocid}'}"
	matching_rule  = "any {resource.type='managementagent'}"
}

resource "oci_identity_policy" "monitored_instances" {
	depends_on     = [oci_identity_dynamic_group.monitored_instances]
	compartment_id = var.tenancy_ocid
	description    = "Instance access - Allow OS Management and Object Storage access for Backup"
	name           = "Monitored-Instances"
	
	statements = [
		# Allow OS Management
		"allow service osms to read instances in tenancy",
		"allow dynamic-group Monitored-Instances to read instance-family in tenancy",
		"allow dynamic-group Monitored-Instances to read volume-family in tenancy",
		"allow dynamic-group Monitored-Instances to use osms-managed-instances in tenancy",
		# Resource discovery and monitoring
		"allow dynamic-group Monitored-Instances to {APPMGMT_MONITORED_INSTANCE_READ, APPMGMT_MONITORED_INSTANCE_ACTIVATE} in tenancy where request.instance.id = target.monitored-instance.id",
		"allow dynamic-group Monitored-Instances to {INSTANCE_UPDATE} in tenancy where request.instance.id = target.instance.id",
		"allow dynamic-group Monitored-Instances to {APPMGMT_WORK_REQUEST_READ, INSTANCE_AGENT_PLUGIN_INSPECT} in tenancy",
		# Allow Execution of commands
		"allow dynamic-group Monitored-Instances to use instance-agent-command-execution-family in tenancy where request.instance.id = target.instance.id",
			# Allow dynamic-group RunCommandDynamicGroup to read objects in compartment ABC where all {target.bucket.name = '<bucket_with_script_file>'}
			# Allow dynamic-group RunCommandDynamicGroup to manage objects in compartment ABC where all {target.bucket.name = '<bucket_for_command_output>'}
		# Allow Management
		"allow dynamic-group Monitored-Instances to manage management-agents in tenancy",
		"allow dynamic-group Monitored-Instances to manage osms-family in tenancy"
			# Allow instance to read more infos for instance
			# "allow dynamic-group Monitored-Instances to read virtual-network-family in tenancy",
			# "allow dynamic-group Monitored-Instances to use logging-family in tenancy",
			# "allow dynamic-group Monitored-Instances to use ons-family in tenancy",
			# "allow dynamic-group Monitored-Instances to use object-family in tenancy",
			# "allow dynamic-group Monitored-Instances to use file-family in tenancy"
	]
}

resource "oci_identity_policy" "monitored_resources" {
	depends_on     = [oci_identity_dynamic_group.monitored_resources]
	compartment_id = var.tenancy_ocid
	description    = "Monitored-Resources - Access for management agents"
	name           = "Monitored-Resources"

	statements = [
		"allow dynamic-group Monitored-Resources to manage management-agents in tenancy",
		"allow dynamic-group Monitored-Resources to use metrics in tenancy"
	]
}

resource "oci_identity_policy" "service_access" {
	compartment_id = var.tenancy_ocid
	description    = "Object Storage Lifecycle Policy - Allow objectstorage service to manage lifecyle of objects in buckets"
	name           = "Service-Access"

	statements = [
		"allow service objectstorage-${var.region} to manage object-family in tenancy"
	]
}

# Delete all objects beginning with /tmp after one day in project bucket
resource "oci_objectstorage_object_lifecycle_policy" "deletion_rule_for_tempfiles" {
	depends_on = [oci_identity_policy.service_access]
	bucket     = var.project_name
	namespace  = data.oci_objectstorage_namespace.get_namespace.namespace

	rules {
		name = "DeletionRule4TempFiles"
		action = "DELETE"
		is_enabled = true
		time_amount = 1
		time_unit = "DAYS"

		object_name_filter {
			inclusion_prefixes = ["tmp/"]
		}
		target = "objects"
	}
}

resource "oci_identity_tag_namespace" "instance_backup_tag_namespace" {
	compartment_id = var.tenancy_ocid
	description    = "Backup Policy for Instance attached volumes"
	name           = "Instance-Backup"
	is_retired     = false
}

resource "oci_identity_tag" "retention_period_tag" {
	description = "Keep the backups for specified number of days (min=1, max=730)"
	name = "RetentionPeriod"
	tag_namespace_id = oci_identity_tag_namespace.instance_backup_tag_namespace.id
	is_cost_tracking = false
	is_retired = false
}

resource "oci_identity_tag" "backup_tag" {
	description = "If disabled - ignore making backups"
	name = "Backup"
	tag_namespace_id = oci_identity_tag_namespace.instance_backup_tag_namespace.id
	is_cost_tracking = false
	validator {
		validator_type = "ENUM"
		values         = ["ENABLED", "DISABLED"]
	}
	is_retired = false
}

resource "oci_identity_tag" "stop_instance_tag" {
	description = "If enabled, the instance is beeing stopped before backup process"
	name = "StopInstance"
	tag_namespace_id = oci_identity_tag_namespace.instance_backup_tag_namespace.id
	is_cost_tracking = false
	validator {
		validator_type = "ENUM"
		values         = ["ENABLED", "DISABLED"]
	}
	is_retired = false
}

resource "oci_identity_tag_namespace" "disk_management_tag_namespace" {
	compartment_id = var.tenancy_ocid
	description    = "Disk Management Policy"
	name           = "Disk-Management"
	is_retired     = false
}

resource "oci_identity_tag" "owner_tag" {
	description = "User and Group of Mountpoint (e.g. opc:opc)"
	name = "Owner"
	tag_namespace_id = oci_identity_tag_namespace.disk_management_tag_namespace.id
	is_cost_tracking = false
	is_retired = false
}

resource "oci_identity_tag" "mountpoint_tag" {
	description = "Mountpoint of disk (e.g. /data)"
	name = "Mountpoint"
	tag_namespace_id = oci_identity_tag_namespace.disk_management_tag_namespace.id
	is_cost_tracking = false
	is_retired = false
}

resource "oci_identity_tag" "filesystem_tag" {
	description = "How the disk has to be formatted"
	name = "Filesystem"
	tag_namespace_id = oci_identity_tag_namespace.disk_management_tag_namespace.id
	is_cost_tracking = false
	validator {
		validator_type = "ENUM"
		values         = ["XFS"]
	}
	is_retired = false
}
