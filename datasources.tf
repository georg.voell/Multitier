# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# Get infos from existion objects

# Get namespace
data "oci_objectstorage_namespace" "get_namespace" {
}

# Get a list of Availability Domains
data "oci_identity_availability_domains" "ads" {
	compartment_id = var.tenancy_ocid
}

# Service Gateway All Services
data "oci_core_services" "service_gateway_all_oci_services" {
	filter {
		name   = "name"
		values = ["All [A-Za-z0-9]+ Services In Oracle Services Network"]
		regex  = true
	}
}

# Service Gateway Object Storage
data "oci_core_services" "service_gateway_object_storage" {
	filter {
		name   = "name"
		values = ["OCI [A-Za-z0-9]+ Object Storage"]
		regex  = true
	}
}

# DNS Resolver
data "oci_core_vcn_dns_resolver_association" "prod_vcn_dns_resolver_association" {
	vcn_id = oci_core_virtual_network.prod_vcn.id
}

# Get Infos about the tenancy
data "oci_identity_tenancy" "tenancy" {
  tenancy_id = var.tenancy_ocid
}

# Get infos about terraform user
data "oci_identity_user" "current_user" {
	user_id = var.user_ocid
}

# Get private ip from bastion
data "oci_core_private_ips" "private_ip_bastion" {
	ip_address = oci_core_instance.prod_bastion.private_ip
	subnet_id  = oci_core_subnet.prod_vcn_sn_dmz.id
}

# Get the tenancy's home region
data "oci_identity_regions" "home_region" {
	filter {
		name   = "key"
		values = [data.oci_identity_tenancy.tenancy.home_region_key]
	}
}

# Get region info from variable region
data "oci_identity_region_subscriptions" "get_region" {
	tenancy_id = var.tenancy_ocid
	filter {
		name   = "region_name"
		values = ["${var.region}"]
		regex  = false
	}
}

# Gets the OCID of the OS image to use
data "oci_core_images" "OL79ImageOCID" {
	compartment_id           = var.compartment_ocid
	operating_system         = "Oracle Linux"
	operating_system_version = "7.9"
	filter {
		name = "display_name"
		values = ["^([a-zA-z]+)-([a-zA-z]+)-([\\.0-9]+)-([\\.0-9-]+)$"]
		regex = true
	}
}
data "oci_core_images" "OAL79ImageOCID" {
	compartment_id           = var.compartment_ocid
	operating_system         = "Oracle Autonomous Linux"
	operating_system_version = "7.9"
}
