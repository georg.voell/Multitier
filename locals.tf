# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

locals {
	# Define some useful constants
	icmp_protocol    = 1
	tcp_protocol     = 6
	udp_protocol     = 17
	all_protocols    = "all"
	anywhere         = "0.0.0.0/0"
	jumphost         = "193.122.5.175/32"
	ssh_port         = 22
	dns_port         = 53
	http_port        = 80
	https_port       = 443
	db_port          = 1521
	iperf3_port      = 5201
	router_ip        = 1
	bastion_ip       = 2
	operator_ip      = 3
	dns_listner_ip   = 4
	dns_forwarder_ip = 5
	
	# Calculate th CIDRs for the subnets - depending on VCN CIDR (please use /22)
	dmz_sn_cidr   = cidrsubnet("${var.PROD_VCN_CIDR}", 3, 0)
	oper_sn_cidr  = cidrsubnet("${var.PROD_VCN_CIDR}", 3, 1)
	app_sn_cidr   = cidrsubnet("${var.PROD_VCN_CIDR}", 2, 1)
	db_sn_cidr    = cidrsubnet("${var.PROD_VCN_CIDR}", 2, 2)
	back_sn_cidr  = cidrsubnet("${var.PROD_VCN_CIDR}", 2, 3)
	
	# Region key for subscribed region where we create the resources e.g. FRA for eu-frankfurt-1
	region_key    = data.oci_identity_region_subscriptions.get_region.region_subscriptions[0].region_key
	
	# Our home region
	home_region   = lookup(data.oci_identity_regions.home_region.regions[0], "name")
}
