# Multi-Tier Architecture

With this example code (Terraform and BASH) you can create a simple [multitier](https://en.wikipedia.org/wiki/Multitier_architecture) network infrastructure automatically on Oracle Cloud Infrastructure (OCI). For more infos about OCI visit: https://www.oracle.com/cloud/

If you don't have an OCI environment yet, you can get one free of charge: https://www.oracle.com/cloud/free/

This demo uses the OCI provider examples (modules) in the Terraform registry: https://registry.terraform.io/providers/hashicorp/oci/latest

[Terraform](https://en.wikipedia.org/wiki/Terraform_(software)) is an open-source [infrastructure as code](https://en.wikipedia.org/wiki/Infrastructure_as_code) software tool created by [HashiCorp](https://www.terraform.io/). Users define and provision data center infrastructure using a declarative configuration language known as HashiCorp Configuration Language (HCL), or optionally JSON.

This sample code can be used on its own or as a base module for an expanded infrastructure. In total, a VCN with four subnets is set up with predefined routing rules and security lists. A public subnet service as a DMZ in which a bastion host is set up. A private oprations subnet with an operator host is also provided. A private subnet can be used to accommodate VMs for applications. Databases can be installed in a further private subnet. Another optional subnet could be used e.g. to set up an ExaCS service (requires 2 subnets for connection).

## Network Architecture

![Network Architecture](./docs/Architecture-v1.jpg)

## Preparation

The required variables are transferred to Terraform in the form of environment variables. Define a file (e.g. profile.bash) with these environment variables (example):
```
# Provider details
export TF_VAR_config_file_profile="DEFAULT"
export TF_VAR_region="eu-frankfurt-1"
export TF_VAR_tenancy_ocid="ocid1.tenancy.oc1..aaaaaaaa42sjquxwmvfckpptnbg2rqrkrawuobqkouyrdqlzrfxhntoppxxx"
export TF_VAR_user_ocid="ocid1.user.oc1..aaaaaaaareltdqvogiysvxtqqrxqu7qfzgam5zwc2euiffcc46dzjc4phxxx"
export TF_VAR_fingerprint="58:b8:08:9a:ec:7a:1f:ec:b6:02:3d:5d:de:43:8f:62"

# Project details
export TF_VAR_project_name="Test"
export TF_VAR_compartment_ocid="ocid1.compartment.oc1..aaaaaaaa6ckavppji77zsrnq72aqp42byx3ry5tmgl4rvfkrz7vsvwzbexxx"
export TF_VAR_label_prefix="none"
export TF_VAR_ssh_private_key_path="~/.ssh/id_rsa"
export TF_VAR_ssh_public_key_path="~/.ssh/id_rsa.pub"
```
The OCI provider is configured to work with an ApiKey - thus oci cli has to be pre-configured and a correct "~/.oci/config" has to be present. Otherwise you'll have to change the "provider.tf" file.

A description of the provider parameters can be found here: https://registry.terraform.io/providers/hashicorp/oci/latest/docs.
The project parameters essentially consist of the project name, the OCID of a compartment (where the infrastructure is set up) and a public-private key pair for the ssh connection to the VMs.

After you have defined these parameters in a file, you can read them into a BASH shell with "source profile.bash". You can download the complete sample code of this project with this command: "git clone https://gitlab.com/georg.voell/multitier.git multitier"

Change to the directory with "cd multitier", change the default variables in "terraform.tfvars" and call the two Terraform commands "terraform init" and "terraform apply". The entire infrastructure can be dismantled again with the command "terraform destroy".
```
source profile.bash
git clone https://gitlab.com/georg.voell/multitier.git multitier
cd multitier
terraform init
terraform apply
```

| [>>](docs/setup.md)
