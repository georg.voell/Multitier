# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# OCI Provider

provider "oci" {
	disable_auto_retries = false
	retry_duration_seconds = 1200

	region = var.region
	tenancy_ocid = var.tenancy_ocid
	user_ocid = var.user_ocid
	fingerprint = var.fingerprint
	
	auth = "ApiKey"	# or "InstancePrincipal" or "SecurityToken"
	config_file_profile = var.config_file_profile

	# private_key_path = ""
	# private_key_password = ""
}
