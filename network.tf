# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# Crate all needed network resourcs

# Create DRG
resource "oci_core_drg" "prod_drg" {
	count = var.DRG_OCID == "" ? 1 : 0
	compartment_id = var.compartment_ocid
	display_name   = var.label_prefix == "none" ? "DRG" : "${var.label_prefix}-DRG"
}

# DRG Attachment
resource "oci_core_drg_attachment" "prod_drg_attachment" {
	drg_id       = var.DRG_OCID == "" ? oci_core_drg.prod_drg[0].id : "${var.DRG_OCID}"
	vcn_id       = oci_core_virtual_network.prod_vcn.id
	display_name = var.label_prefix == "none" ? "DRG-Attachment" : "${var.label_prefix}-DRG-Attachment"
}

# Create PROD VCN
resource "oci_core_virtual_network" "prod_vcn" {
	compartment_id = var.compartment_ocid
	cidr_block     = var.PROD_VCN_CIDR
	dns_label      = var.label_prefix == "none" ? "vcn${local.region_key}" : "${var.label_prefix}${local.region_key}"
	display_name   = var.label_prefix == "none" ? "VCN ${local.region_key}" : "${var.label_prefix}-VCN ${local.region_key}"
}

# Create Internet Gateway
resource "oci_core_internet_gateway" "prod_igw" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "IGW" : "${var.label_prefix}-IGW"
}

# Create NAT Gateway
resource "oci_core_nat_gateway" "prod_ngw" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "NGW" : "${var.label_prefix}-NGW"
}

# Create Service Gateway
resource "oci_core_service_gateway" "prod_sgw" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "SGW" : "${var.label_prefix}-SGW"
	services {
		service_id = data.oci_core_services.service_gateway_all_oci_services.services[0].id
	}
}

# Route Tables
resource "oci_core_route_table" "prod_route_table_priv" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "Private Route Table for VCN" : "${var.label_prefix}-Private Route Table for VCN"
	route_rules {
		network_entity_id = var.DRG_OCID == "" ? oci_core_drg.prod_drg[0].id : "${var.DRG_OCID}"
		description       = "Route to OnPrem and all VCN"
		destination       = var.ONPREM_CIDR
		destination_type  = "CIDR_BLOCK"
	}
	route_rules {
		network_entity_id = oci_core_nat_gateway.prod_ngw.id
		description       = "Get updates"
		destination       = local.anywhere
		destination_type  = "CIDR_BLOCK"
	}
	route_rules {
		network_entity_id = oci_core_service_gateway.prod_sgw.id
		description       = "Access to Service Gateway"
		destination       = data.oci_core_services.service_gateway_all_oci_services.services[0]["cidr_block"]
		destination_type  = "SERVICE_CIDR_BLOCK"
	}
}
resource "oci_core_route_table" "prod_route_table_pub" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "Public Route Table for VCN" : "${var.label_prefix}-Public Route Table for VCN"
	route_rules {
		network_entity_id = var.DRG_OCID == "" ? oci_core_drg.prod_drg[0].id : "${var.DRG_OCID}"
		description       = "Route to OnPrem and all VCN"
		destination       = var.ONPREM_CIDR
		destination_type  = "CIDR_BLOCK"
	}
	route_rules {
		network_entity_id = oci_core_internet_gateway.prod_igw.id
		description       = "Internet access"
		destination       = local.anywhere
		destination_type  = "CIDR_BLOCK"
	}
	route_rules {
		network_entity_id = oci_core_service_gateway.prod_sgw.id
		description       = "Access to Object Storage"
		destination       = data.oci_core_services.service_gateway_object_storage.services[0]["cidr_block"]
		destination_type  = "SERVICE_CIDR_BLOCK"
	}
}

# DMZ Subnet
resource "oci_core_subnet" "prod_vcn_sn_dmz" {
	cidr_block                 = local.dmz_sn_cidr
	compartment_id             = var.compartment_ocid
	vcn_id                     = oci_core_virtual_network.prod_vcn.id
	display_name               = var.label_prefix == "none" ? "DMZ Subnet" : "${var.label_prefix}-DMZ Subnet"
	dns_label                  = "dmz"
	prohibit_public_ip_on_vnic = false
	route_table_id             = oci_core_route_table.prod_route_table_pub.id
	security_list_ids          = [ oci_core_security_list.prod_security_list_pub.id ]
}

# Operations Subnet
resource "oci_core_subnet" "prod_vcn_sn_oper" {
	cidr_block                 = local.oper_sn_cidr
	compartment_id             = var.compartment_ocid
	vcn_id                     = oci_core_virtual_network.prod_vcn.id
	display_name               = var.label_prefix == "none" ? "Operations Subnet" : "${var.label_prefix}-Operations Subnet"
	dns_label                  = "oper"
	prohibit_public_ip_on_vnic = true
	route_table_id             = oci_core_route_table.prod_route_table_priv.id
	security_list_ids          = [ oci_core_security_list.prod_security_list_priv.id ]
}

# Application Subnet
resource "oci_core_subnet" "prod_vcn_sn_app" {
	cidr_block                 = local.app_sn_cidr
	compartment_id             = var.compartment_ocid
	vcn_id                     = oci_core_virtual_network.prod_vcn.id
	display_name               = var.label_prefix == "none" ? "Application Subnet" : "${var.label_prefix}-Application Subnet"
	dns_label                  = "app"
	prohibit_public_ip_on_vnic = true
	route_table_id             = oci_core_route_table.prod_route_table_priv.id
	security_list_ids          = [ oci_core_security_list.prod_security_list_priv.id ]
}

# Database Subnet
resource "oci_core_subnet" "prod_vcn_sn_database" {
	cidr_block                 = local.db_sn_cidr
	compartment_id             = var.compartment_ocid
	vcn_id                     = oci_core_virtual_network.prod_vcn.id
	display_name               = var.label_prefix == "none" ? "Database Subnet" : "${var.label_prefix}-Database Subnet"
	dns_label                  = "db"
	prohibit_public_ip_on_vnic = true
	route_table_id             = oci_core_route_table.prod_route_table_priv.id
	security_list_ids          = [ oci_core_security_list.prod_security_list_priv.id ]
}

# Backup Subnet
resource "oci_core_subnet" "prod_vcn_sn_backup" {
	cidr_block                 = local.back_sn_cidr
	compartment_id             = var.compartment_ocid
	vcn_id                     = oci_core_virtual_network.prod_vcn.id
	display_name               = var.label_prefix == "none" ? "Backup Subnet" : "${var.label_prefix}-Backup Subnet"
	dns_label                  = "back"
	prohibit_public_ip_on_vnic = true
	route_table_id             = oci_core_route_table.prod_route_table_priv.id
	security_list_ids          = [ oci_core_security_list.prod_security_list_priv.id ]
}
