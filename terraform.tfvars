# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# CIDR Ranges
ONPREM_CIDR       = "10.0.0.0/8"
PROD_VCN_CIDR     = "192.168.0.0/22"

# If DRG already exists, place OCID here - or leave empty to create a new DRG
DRG_OCID          = ""

# Set to false, if you don't want to use OS Management Agent
USE_OS_MGMT_AGENT = true

# Uncomment the next variables if you want to change the defaults

# If you want to use a label
# label_prefix      = "PROD"

# If you want to deploy your resources in an other region then the home region
# region            = "eu-amsterdam-1"
