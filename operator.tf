# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# Create operator host on compute

# Create operator instance
resource "oci_core_instance" "prod_operator" {
	compartment_id                      = var.compartment_ocid
	availability_domain                 = data.oci_identity_availability_domains.ads.availability_domains[0].name
	shape                               = "VM.Standard2.1"
	display_name                        = "Operator"
	preserve_boot_volume                = false
	is_pv_encryption_in_transit_enabled = true

	source_details {
		source_id   = data.oci_core_images.OL79ImageOCID.images[0].id
		source_type = "image"
	}
	
	agent_config {
		are_all_plugins_disabled = false
		is_management_disabled   = false
		is_monitoring_disabled   = false
		plugins_config {
			name          = "Vulnerability Scanning"
			desired_state = "DISABLED"
		}
		plugins_config {
			name          = "OS Management Service Agent"
			desired_state = var.USE_OS_MGMT_AGENT ? "ENABLED" : "DISABLED"
		}
		plugins_config {
			name          = "Management Agent"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Custom Logs Monitoring"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Compute Instance Run Command"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Compute Instance Monitoring"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Block Volume Management"
			desired_state = "DISABLED"
		}
		plugins_config {
			name          = "Bastion"
			desired_state = "ENABLED"
		}
	}
	
	platform_config {
		type                               = "INTEL_VM"
		is_measured_boot_enabled           = true
		is_secure_boot_enabled             = true
		is_trusted_platform_module_enabled = true
	}
	
	instance_options {
		are_legacy_imds_endpoints_disabled = true
	}
	
	create_vnic_details {
		hostname_label   = "operator"
		private_ip       = cidrhost("${local.oper_sn_cidr}", local.operator_ip)
		assign_public_ip = false
		subnet_id        = oci_core_subnet.prod_vcn_sn_oper.id
	}
	
	metadata = {
		ssh_authorized_keys = file("${var.ssh_public_key_path}")
		user_data           = "${base64encode(file("./artifacts/bootstrap"))}"
	}

	lifecycle {
		ignore_changes = [defined_tags, agent_config]
	}
	
	defined_tags = {
		"Oracle-Tags.CreatedBy"="${data.oci_identity_user.current_user.name}",
		"Oracle-Tags.CreatedOn"=timestamp(),
		"Instance-Backup.RetentionPeriod"="30",
		"Instance-Backup.Backup"="DISABLED",
		"Instance-Backup.StopInstance"="ENABLED"
	}
}

# Display the OCID of operator instance and save it in outputs
output "operator_ocid" {
	description = "OCID of operator host"
	value       = oci_core_instance.prod_operator.id
}

# Display the private ip of operator instance and save it in outputs
output "operator_private_ip" {
	description = "Private ip address of operator host"
	value       = oci_core_instance.prod_operator.private_ip
}
