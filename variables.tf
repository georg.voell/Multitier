# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# OCI provider identity parameters
variable "config_file_profile" {
	# Filled via "source ${HOME}/.admintools"
	description = "Profile from ~.oci/config to use"
	type        = string
	default     = "DEFAULT"
}
variable "region" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "The OCI region where resources will be created"
	type        = string
}
variable "tenancy_ocid" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "Tenancy OCID where to create the resources"
	type        = string
}
variable "user_ocid" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "OCID of user that terraform will use to create the resources"
	type        = string
}
variable "fingerprint" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "Fingerprint of the api key attached to the user"
	type        = string
}

# Project OCI parameters
variable "project_name" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "The projct / compartment name"
	type        = string
}
variable "compartment_ocid" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "The compartment OCID where to create the resources"
	type        = string
}
variable "label_prefix" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "Use label 'none' to avoid labeling - otherwise most resources will use this label in display name"
	type        = string
}
variable "ssh_private_key_path" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "Private SSH key to access the instance via ssh"
	type        = string
}
variable "ssh_public_key_path" {
	# Filled via "source ${HOME}/.admintools" - no default value
	description = "Public SSH key to be included in the ~/.ssh/authorized_keys file for the default user on the instance"
	type        = string
}

# Other parameters
variable "ONPREM_CIDR" {
	# Set by "terraform.tfvars"
	description = "The CIDR that is being used by the customer OnPrem"
	type        = string
	default     = "10.0.0.0/8"
}
variable "PROD_VCN_CIDR" {
	# Set by "terraform.tfvars"
	description = "The CIDR that is being used by the VCN in the cloud. Has to be /22"
	type        = string
	default     = "10.0.0.0/22"
}
variable "DRG_OCID" {
	# Set by "terraform.tfvars"
	description = "If a DRG already exists and has to b used - specify the OCID of this DRG. If empty, DRG will be created"
	type        = string
	default     = ""
}
variable "USE_OS_MGMT_AGENT" {
	# Set by "terraform.tfvars"
	description = "Set to true, if you want to use OS Management Agent"
	type        = bool
	default     = false
}
