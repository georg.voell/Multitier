# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# Network Security (Scurity Lists and Network Security Groups)

# Private Security List for all private Subnets
resource "oci_core_security_list" "prod_security_list_priv" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "Private Security List" : "${var.label_prefix}-Private Security List"
	
	egress_security_rules {
		destination      = local.anywhere
		protocol         = local.all_protocols
		description      = "All all for egress"
		destination_type = "CIDR_BLOCK"
		stateless        = false
	}
	ingress_security_rules {
		protocol    = local.tcp_protocol
		source      = local.anywhere
		description = "SSH Login from anywhere"
		source_type = "CIDR_BLOCK"
		stateless   = false
		tcp_options {
			max = local.ssh_port
			min = local.ssh_port
		}
	}
	ingress_security_rules {
		protocol    = local.icmp_protocol
		source      = local.anywhere
		description = "Allow ping from anywhere"
		source_type = "CIDR_BLOCK"
		stateless   = false
	}
	ingress_security_rules {
		protocol    = local.icmp_protocol
		source      = local.anywhere
		description = "Enable Path Maximum Transmission Unit Discovery (PMTUD)"
		source_type = "CIDR_BLOCK"
		stateless   = false
			icmp_options {
				type = 3
				code = 4
			}
	}
	ingress_security_rules {
		protocol    = local.icmp_protocol
		source      = var.PROD_VCN_CIDR
		description = "Destination is reachable from VCN"
		source_type = "CIDR_BLOCK"
		stateless   = false
			icmp_options {
				type = 3
			}
	}
}

# Public Security List for all public Subnets
resource "oci_core_security_list" "prod_security_list_pub" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "Public Security List" : "${var.label_prefix}-Public Security List"
	
	egress_security_rules {
		destination      = local.anywhere
		protocol         = local.all_protocols
		description      = "All all for egress"
		destination_type = "CIDR_BLOCK"
		stateless        = false
	}
	ingress_security_rules {
		protocol    = local.tcp_protocol
		source      = local.anywhere
		description = "SSH Login from anywhere"
		source_type = "CIDR_BLOCK"
		stateless   = false
		tcp_options {
			max = local.ssh_port
			min = local.ssh_port
		}
	}
	ingress_security_rules {
		protocol    = local.icmp_protocol
		source      = local.anywhere
		description = "Enable Path Maximum Transmission Unit Discovery (PMTUD)"
		source_type = "CIDR_BLOCK"
		stateless   = false
			icmp_options {
				type = 3
				code = 4
			}
	}
	ingress_security_rules {
		protocol    = local.icmp_protocol
		source      = var.PROD_VCN_CIDR
		description = "Destination is reachable from VCN"
		source_type = "CIDR_BLOCK"
		stateless   = false
			icmp_options {
				type = 3
			}
	}
}

# Network Security Group DNS
resource "oci_core_network_security_group" "prod_nsg_dns" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name = var.label_prefix == "none" ? "DNS" : "${var.label_prefix}-DNS"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_dns_tcp" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_dns.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allows DNS TCP"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = local.dns_port
			min = local.dns_port
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_dns_udp" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_dns.id
	direction                 = "INGRESS"
	protocol                  = local.udp_protocol
	description               = "Allows DNS UDP"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	udp_options {
		destination_port_range {
			max = local.dns_port
			min = local.dns_port
		}
	}
}

# Network Security Group HTTP
resource "oci_core_network_security_group" "prod_nsg_http" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name = var.label_prefix == "none" ? "HTTP" : "${var.label_prefix}-HTTP"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_http_tcp" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_http.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allows HTTP TCP"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = local.http_port
			min = local.http_port
		}
	}
}

# Network Security Group HTTPS
resource "oci_core_network_security_group" "prod_nsg_https" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name = var.label_prefix == "none" ? "HTTPS" : "${var.label_prefix}-HTTPS"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_https_tcp" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_https.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allows HTTPS TCP"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = local.https_port
			min = local.https_port
		}
	}
}

# Network Security Group Cockpit
resource "oci_core_network_security_group" "prod_nsg_cockpit" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name = var.label_prefix == "none" ? "Cockpit" : "${var.label_prefix}-Cockpit"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_cockpit_tcp" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_cockpit.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allows Cockpit TCP"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = "9090"
			min = "9090"
		}
	}
}

# Network Security Group NFS
resource "oci_core_network_security_group" "prod_nsg_nfs" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "NFS" : "${var.label_prefix}-NFS"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_dns_tcp1" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_nfs.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Portmapper"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = "111"
			min = "111"
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_dns_udp1" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_nfs.id
	direction                 = "INGRESS"
	protocol                  = local.udp_protocol
	description               = "Portmapper"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	udp_options {
		destination_port_range {
			max = "111"
			min = "111"
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_dns_tcp2" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_nfs.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "NFS Service"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = "2050"
			min = "2048"
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_dns_udp2" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_nfs.id
	direction                 = "INGRESS"
	protocol                  = local.udp_protocol
	description               = "NFS Service"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	udp_options {
		destination_port_range {
			max = "2048"
			min = "2048"
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_dns_tcp3" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_nfs.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "mountd"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = "20048"
			min = "20048"
		}
	}
}

# Network Security Group Exa-Cient
resource "oci_core_network_security_group" "prod_nsg_exa_client" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "EXA-Client" : "${var.label_prefix}-EXA-Client"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_em_agent" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_exa_client.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Enterprise Manager Cloud Control Agent"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = "3872"
			min = "3872"
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_ons_fan" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_exa_client.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allow ONS- and FAN-Traffic from Client Subnet"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = "6200"
			min = "6200"
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_sql" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_exa_client.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allow SQL*NET-Traffic from Client Subnet"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = local.db_port
			min = local.db_port
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_exa_ssh" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_exa_client.id
	direction                 = "EGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allow all TCP-Traffic from Client Subnet"
	destination               = local.anywhere
	destination_type          = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = local.ssh_port
			min = local.ssh_port
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_exa_all" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_exa_client.id
	direction                 = "EGRESS"
	protocol                  = local.all_protocols
	description               = "Allow all Egress-Traffic (connection to Oracle YUM-Repositorys)"
	destination               = local.anywhere
	destination_type          = "CIDR_BLOCK"
	stateless                 = false
}

# Network Security Group Exa-Backup
resource "oci_core_network_security_group" "prod_nsg_exa_backup" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "EXA-Backup" : "${var.label_prefix}-EXA-Backup"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_exabackup_allow-objectstorage" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_exa_backup.id
	direction                 = "EGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allow all SGW connections (Object Storage)"
	destination_type          = "SERVICE_CIDR_BLOCK"
	destination               = data.oci_core_services.service_gateway_all_oci_services.services[0]["cidr_block"]
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = local.https_port
			min = local.https_port
		}
	}
}

# Network Security Group iperf3
resource "oci_core_network_security_group" "prod_nsg_iperf3" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "iperf3 (only for testing)" : "${var.label_prefix}-iperf3 (only for testing)"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_iperf3_tcp" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_iperf3.id
	direction                 = "INGRESS"
	protocol                  = local.tcp_protocol
	description               = "Allows iperf3 TCP"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	tcp_options {
		destination_port_range {
			max = local.iperf3_port
			min = local.iperf3_port
		}
	}
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_iperf3_udp" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_iperf3.id
	direction                 = "INGRESS"
	protocol                  = local.udp_protocol
	description               = "Allows iperf3 UDP"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false

	udp_options {
		destination_port_range {
			max = local.iperf3_port
			min = local.iperf3_port
		}
	}
}

# Network Security Group ALLOW-ALL
resource "oci_core_network_security_group" "prod_nsg_allow_all" {
	compartment_id = var.compartment_ocid
	vcn_id         = oci_core_virtual_network.prod_vcn.id
	display_name   = var.label_prefix == "none" ? "Allow All (only for testing)" : "${var.label_prefix}-Allow All (only for testing)"
}
resource "oci_core_network_security_group_security_rule" "rule_nsg_allow_all" {
	network_security_group_id = oci_core_network_security_group.prod_nsg_allow_all.id
	direction                 = "INGRESS"
	protocol                  = local.all_protocols
	description               = "Allows All Traffic"
	source                    = local.anywhere
	source_type               = "CIDR_BLOCK"
	stateless                 = false
}
