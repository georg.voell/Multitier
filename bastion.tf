# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# Create a bastion host on compute

# Reserve a public ip for bastion host
resource "oci_core_public_ip" "prod_public_ip_bastion" {
	compartment_id = var.compartment_ocid
	display_name   = "Bastion"
	lifetime       = "RESERVED"
	private_ip_id  = data.oci_core_private_ips.private_ip_bastion.private_ips[0]["id"]
}

# Create bastion instance
resource "oci_core_instance" "prod_bastion" {
	compartment_id                      = var.compartment_ocid
	availability_domain                 = data.oci_identity_availability_domains.ads.availability_domains[0].name
	shape                               = "VM.Standard2.1"
	display_name                        = "Bastion"
	preserve_boot_volume                = false
	is_pv_encryption_in_transit_enabled = true

	source_details {
		source_id   = data.oci_core_images.OAL79ImageOCID.images[0].id
		source_type = "image"
	}

	agent_config {
		are_all_plugins_disabled = false
		is_management_disabled   = false
		is_monitoring_disabled   = false
		plugins_config {
			name          = "Vulnerability Scanning"
			desired_state = "DISABLED"
		}
		plugins_config {
			name          = "Oracle Autonomous Linux"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "OS Management Service Agent"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Management Agent"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Custom Logs Monitoring"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Compute Instance Run Command"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Compute Instance Monitoring"
			desired_state = "ENABLED"
		}
		plugins_config {
			name          = "Block Volume Management"
			desired_state = "DISABLED"
		}
		plugins_config {
			name          = "Bastion"
			desired_state = "DISABLED"
		}
	}
	platform_config {
		type                               = "INTEL_VM"
		is_measured_boot_enabled           = true
		is_secure_boot_enabled             = true
		is_trusted_platform_module_enabled = true
	}
	
	instance_options {
		are_legacy_imds_endpoints_disabled = true
	}
	
	create_vnic_details {
		hostname_label   = "bastion"
		private_ip       = cidrhost("${local.dmz_sn_cidr}", local.bastion_ip)
		assign_public_ip = false
		subnet_id        = oci_core_subnet.prod_vcn_sn_dmz.id
	}
	
	metadata = {
		ssh_authorized_keys = file("${var.ssh_public_key_path}")
		# user_data           = "${base64encode(file("./artifacts/bootstrap"))}"
	}
	
	lifecycle {
		ignore_changes = [defined_tags, agent_config]
	}
	
	defined_tags = {
		"Oracle-Tags.CreatedBy"="${data.oci_identity_user.current_user.name}",
		"Oracle-Tags.CreatedOn"=timestamp(),
		"Instance-Backup.RetentionPeriod"="30",
		"Instance-Backup.Backup"="DISABLED",
		"Instance-Backup.StopInstance"="ENABLED"
	}
}

# Display the OCID of bastion instance and save it in outputs
output "bastion_ocid" {
	description = "OCID of bastion host"
	value       = oci_core_instance.prod_bastion.id
}

# Display the public ip of bastion instance and save it in outputs
output "bastion_public_ip" {
	description = "Public ip address of bastion host"
	value       = oci_core_public_ip.prod_public_ip_bastion.ip_address
}

# Display the private ip of bastion instance and save it in outputs
output "bastion_private_ip" {
	description = "Private ip address of bastion host"
	value       = oci_core_instance.prod_bastion.private_ip
}
