# Copyright 2021, Oracle Corporation and/or affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl
# Author: Georg Voell - georg.voell@oracle.com

# yum update -y
# systemctl restart oracle-cloud-agent.service
# sudo systemctl stop oracle-cloud-agent.service
# sudo systemctl disable oracle-cloud-agent.service

# ssh -i ~/.ssh/keys/oraseemeadetce3/id_rsa.pk8 -o ProxyCommand="ssh -i ~/.ssh/keys/oraseemeadetce3/id_rsa.pk8 -W %h:%p -p 22 ocid1.bastionsession.oc1.eu-frankfurt-1.amaaaaaa55ifkyya5mwqrxv47n7lgnvcavnclz22srjtoidc55w5imenahxa@host.bastion.eu-frankfurt-1.oci.oraclecloud.com" -p 22 opc@10.33.0.130
# ssh -i ~/.ssh/keys/oraseemeadetce3/id_rsa.pk8 -o ProxyCommand="ssh -i ~/.ssh/keys/oraseemeadetce3/id_rsa.pk8 -W %h:%p -p 22 ocid1.bastionsession.oc1.eu-frankfurt-1.amaaaaaa55ifkyyabtdmf3jb7tfqikckydqhw5vebocyd4vvfzzhx6gdq5fq@host.bastion.eu-frankfurt-1.oci.oraclecloud.com" -p 22 opc@10.33.0.131

# Create bastion service
resource "oci_bastion_bastion" "prod_bastion" {
	# depends_on                    = [oci_core_instance.prod_operator]
	bastion_type                  = "STANDARD"
	compartment_id                = var.compartment_ocid
	target_subnet_id              = oci_core_subnet.prod_vcn_sn_oper.id
	client_cidr_block_allow_list  = ["${local.anywhere}"]
	name                          = "Bastion"
	max_session_ttl_in_seconds    = 10800	# 3 hours
	# private_endpoint_ip_address   = cidrhost("${local.oper_sn_cidr}", local.bastion_ip)
}

# Display the private ip of bastion service and save it in outputs
output "bastion_service_ip" {
	description = "private ip address (endpoint) of bastion service"
	value       = oci_bastion_bastion.prod_bastion.private_endpoint_ip_address
}

# Display the OCID of bastion service and save it in outputs
output "bastion_service_ocid" {
	description = "Private ip address (endpoint) of bastion service"
	value       = oci_bastion_bastion.prod_bastion.id
}
